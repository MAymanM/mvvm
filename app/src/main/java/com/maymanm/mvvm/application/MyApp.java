package com.maymanm.mvvm.application;

import android.app.Application;
import android.content.Context;

/**
 * Created by MahmoudAyman on 27/03/2019.
 */
public class MyApp extends Application {
    private static MyApp instance;
    public static MyApp getInstance() {
        return instance;
    }

    public static Context getContext(){
        return instance;
        // or return instance.getApplicationContext();
    }

    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();
    }

}
