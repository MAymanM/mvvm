package com.maymanm.mvvm.model.room.entity;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by MahmoudAyman on 3/19/2019.
 **/
//@Entity(tableName = "new name")
@Entity
public class Note {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String title;
    private String description;
    //if we want to change name of column
    //@ColumnInfo(name = "any new name")
    private int priority;
    
    //we dont add id cuz it will added auto
    public Note(String title, String description, int priority) {
        this.title = title;
        this.description = description;
        this.priority = priority;
    }

    //important ... if not compile time error will be shown
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getPriority() { return priority; }
}
