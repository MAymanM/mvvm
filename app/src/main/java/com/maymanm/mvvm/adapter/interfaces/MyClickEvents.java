package com.maymanm.mvvm.adapter.interfaces;

import com.maymanm.mvvm.model.room.entity.Note;

/**
 * Created by MahmoudAyman on 3/28/2019.
 **/
public interface MyClickEvents {
    void onItemClick(Note note, int pos);
}
