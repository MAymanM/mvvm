package com.maymanm.mvvm.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.maymanm.mvvm.R;

import java.util.List;
import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class AddEditNoteActivity extends AppCompatActivity {

    @BindViews({R.id.titleET, R.id.descriptionET})
    List<TextInputEditText> editTexts;
    TextInputEditText titleET, descriptionET;
    @BindView(R.id.numberPicker)
    NumberPicker numberPicker;

    public static final String EXTRA_ID = "com.maymanm.mvvm.view.activity.EXTRA_ID";
    public static final String EXTRA_TITLE = "com.maymanm.mvvm.view.activity.EXTRA_TITLE";
    public static final String EXTRA_DESCRIPTION = "com.maymanm.mvvm.view.activity.EXTRA_DESCRIPTION";
    public static final String EXTRA_PRIORITY = "com.maymanm.mvvm.view.activity.EXTRA_PRIORITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);
        ButterKnife.bind(this);
        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_close);
        initUI();
        getData();
    }

    private void getData() {
        Intent data = getIntent();
        if (data.hasExtra(EXTRA_ID)) {
            setTitle("Edit Note");
            titleET.setText(data.getStringExtra(EXTRA_TITLE));
            descriptionET.setText(data.getStringExtra(EXTRA_DESCRIPTION));
            numberPicker.setValue(data.getIntExtra(EXTRA_PRIORITY, 1));
        } else
            setTitle("Add Note");
    }

    private void initUI() {
        titleET = editTexts.get(0);
        descriptionET = editTexts.get(1);

        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(20);
    }


    private void saveNote() {
        if (isEmptyEditTxt(titleET)) {
            titleET.setError(getString(R.string.error_empty_title));
            return;
        }

        if (isEmptyEditTxt(descriptionET)) {
            descriptionET.setError(getString(R.string.error_empty_description));
            return;
        }

        String title = titleET.getText().toString();
        String description = descriptionET.getText().toString();
        int priority = numberPicker.getValue();

        Intent data = new Intent();
        data.putExtra(EXTRA_TITLE, title);
        data.putExtra(EXTRA_DESCRIPTION, description);
        data.putExtra(EXTRA_PRIORITY, priority);

        int id = getIntent().getIntExtra(EXTRA_ID, -1);
        if (id != -1) {
            data.putExtra(EXTRA_ID, id);
        }

        setResult(RESULT_OK, data);
        finish();
    }

    private boolean isEmptyEditTxt(TextInputEditText editText) {
        return editText.getText().toString().trim().length() == 0;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_note_menu, menu);
        return true; // if false menu will not displayed
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_note:
                saveNote();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
