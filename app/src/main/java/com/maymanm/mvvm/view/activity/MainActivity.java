package com.maymanm.mvvm.view.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.maymanm.mvvm.R;
import com.maymanm.mvvm.adapter.interfaces.MyClickEvents;
import com.maymanm.mvvm.adapter.parent.NoteAdapter;
import com.maymanm.mvvm.application.MyApp;
import com.maymanm.mvvm.model.room.entity.Note;
import com.maymanm.mvvm.viewmodel.NoteViewModel;

import static android.widget.Toast.LENGTH_SHORT;
import static com.maymanm.mvvm.view.activity.AddEditNoteActivity.EXTRA_DESCRIPTION;
import static com.maymanm.mvvm.view.activity.AddEditNoteActivity.EXTRA_ID;
import static com.maymanm.mvvm.view.activity.AddEditNoteActivity.EXTRA_PRIORITY;
import static com.maymanm.mvvm.view.activity.AddEditNoteActivity.EXTRA_TITLE;

public class MainActivity extends AppCompatActivity implements MyClickEvents {
    private static final int ADD_NOTE_REQUEST = 1;
    private static final int EDIT_NOTE_REQUEST = 2;
    private NoteViewModel noteViewModel;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private NoteAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initRecyclerView();

        noteViewModel = ViewModelProviders.of(this).get(NoteViewModel.class);
        noteViewModel.getAllNotes().observe(this, notes -> {
            //update RecyclerView
            adapter.submitList(notes);
        });


    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        //you should set this to true if you u know that recyclerView size wont change(it increases the performance of recyclerView)
        recyclerView.setHasFixedSize(true);
        adapter = new NoteAdapter(this);
        recyclerView.setAdapter(adapter);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.END) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                noteViewModel.delete(adapter.getCurrentNode(viewHolder.getAdapterPosition()));
                Toast.makeText(MainActivity.this, "deleted", LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(recyclerView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_all:
                noteViewModel.deleteAllNotes();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.fab)
    void onFabClick() {
        Intent intent = new Intent(MyApp.getContext().getApplicationContext(), AddEditNoteActivity.class);
        startActivityForResult(intent, ADD_NOTE_REQUEST);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ADD_NOTE_REQUEST:
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        String title = data.getStringExtra(EXTRA_TITLE);
                        String desc = data.getStringExtra(EXTRA_DESCRIPTION);
                        int priority = data.getIntExtra(EXTRA_PRIORITY, 1);


                        Note note = new Note(title, desc, priority);
                        noteViewModel.insert(note);
                        Toast.makeText(this, "note saved", LENGTH_SHORT).show();
                    }

                }
                break;
            case EDIT_NOTE_REQUEST:
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        int id = data.getIntExtra(EXTRA_ID, -1);
                        if (id == -1) {
                            Toast.makeText(this, "note can't be updated", LENGTH_SHORT).show();
                        } else {
                            String title = data.getStringExtra(EXTRA_TITLE);
                            String desc = data.getStringExtra(EXTRA_DESCRIPTION);
                            int priority = data.getIntExtra(EXTRA_PRIORITY, 1);

                            Note note = new Note(title, desc, priority);
                            note.setId(id);
                            noteViewModel.update(note);
                            Toast.makeText(this, "note updated", LENGTH_SHORT).show();
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onItemClick(Note note, int pos) {
        Intent intent = new Intent(MyApp.getContext().getApplicationContext(), AddEditNoteActivity.class);
        intent.putExtra(EXTRA_ID, note.getId());
        intent.putExtra(EXTRA_TITLE, note.getTitle());
        intent.putExtra(EXTRA_DESCRIPTION, note.getDescription());
        intent.putExtra(EXTRA_PRIORITY, note.getPriority());
        startActivityForResult(intent, EDIT_NOTE_REQUEST);
    }
}
