package com.maymanm.mvvm.adapter.viewholder;

import android.view.View;
import android.widget.TextView;

import com.maymanm.mvvm.R;
import com.maymanm.mvvm.adapter.interfaces.MyClickEvents;
import com.maymanm.mvvm.model.room.entity.Note;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindViews;
import butterknife.ButterKnife;

import static androidx.recyclerview.widget.RecyclerView.NO_POSITION;

/**
 * Created by MahmoudAyman on 26/03/2019.
 */
public class NoteHolder extends RecyclerView.ViewHolder {

    @BindViews({R.id.priorityTV, R.id.titleTV, R.id.descriptionTV})
    List<TextView> textViewList;
    private TextView priorityTV, titleTV, descriptionTV;

    public NoteHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        priorityTV = textViewList.get(0);
        titleTV = textViewList.get(1);
        descriptionTV = textViewList.get(2);
    }

    public void onBind(Note currentNode, MyClickEvents myClickEvents) {
        priorityTV.setText(String.valueOf(currentNode.getPriority()));
        titleTV.setText(currentNode.getTitle());
        descriptionTV.setText(currentNode.getDescription());
        itemView.setOnClickListener(v -> {
            if (myClickEvents != null && getAdapterPosition() != NO_POSITION) {
                myClickEvents.onItemClick(currentNode, getAdapterPosition());
            }
        });
    }

}
