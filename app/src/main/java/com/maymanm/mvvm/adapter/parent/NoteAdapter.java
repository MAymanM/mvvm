package com.maymanm.mvvm.adapter.parent;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.maymanm.mvvm.R;
import com.maymanm.mvvm.adapter.interfaces.MyClickEvents;
import com.maymanm.mvvm.adapter.viewholder.NoteHolder;
import com.maymanm.mvvm.model.room.entity.Note;

import java.security.InvalidParameterException;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncDifferConfig;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by MahmoudAyman on 26/03/2019.
 */
public class NoteAdapter extends ListAdapter<Note, NoteHolder> {
    private String TAG = getClass().getName();
    private MyClickEvents myClickEvents;

    public NoteAdapter(MyClickEvents myClickEvents) {
        super(DIFF_CALLBACK);
        this.myClickEvents = myClickEvents;
    }

    private static final DiffUtil.ItemCallback<Note> DIFF_CALLBACK = new DiffUtil.ItemCallback<Note>() {
        @Override
        public boolean areItemsTheSame(@NonNull Note oldItem, @NonNull Note newItem) {
            //this will return true cuz the id is the unique part of Note.class
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Note oldItem, @NonNull Note newItem) {
            return oldItem.getTitle().equals(newItem.getTitle()) &&
                    oldItem.getDescription().equals(newItem.getDescription()) &&
                    oldItem.getPriority() == newItem.getPriority();
        }
    };

    @NonNull
    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //parent.getContext() is the context of the activity/fragment the the recyclerView is into.
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.node_item, parent, false);
        return new NoteHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteHolder holder, int position) {
            holder.onBind(getItem(position), myClickEvents);
    }

    public Note getCurrentNode(int pos) {
        return getItem(pos);
    }

}
