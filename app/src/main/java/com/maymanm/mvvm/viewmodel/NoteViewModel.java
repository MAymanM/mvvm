package com.maymanm.mvvm.viewmodel;

import android.app.Application;

import com.maymanm.mvvm.model.room.entity.Note;
import com.maymanm.mvvm.repository.NoteRepository;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

/**
 * Created by MahmoudAyman on 24/03/2019.
 */
public class NoteViewModel extends AndroidViewModel {
    private NoteRepository repository;
    private LiveData<List<Note>> allNotes;


    public NoteViewModel(@NonNull Application application) {
        super(application);
        repository = new NoteRepository(application);
        allNotes = repository.getAllNotes();
    }

    public void  insert (Note note){
        repository.insert(note);
    }
    public void  update (Note note){
        repository.update(note);
    }
    public void  delete (Note note){
        repository.delete(note);
    }
    public void  deleteAllNotes (){
        repository.deleteAllNotes();
    }

    public LiveData<List<Note>> getAllNotes() {
        return allNotes;
    }
}
