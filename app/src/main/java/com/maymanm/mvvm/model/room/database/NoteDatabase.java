package com.maymanm.mvvm.model.room.database;

import android.content.Context;
import android.os.AsyncTask;

import com.maymanm.mvvm.model.room.entity.Note;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

/**
 * Created by MahmoudAyman on 3/19/2019.
 **/
@Database(entities = {Note.class}, version = 1, exportSchema = false)
public abstract class NoteDatabase extends RoomDatabase {

    //(singleton)
    //static constructor to stop making multiple instances.
    private static NoteDatabase instance;

    //method id 'abstract' cuz its not have a body.
    //we don't need a body cuz Room take care of our code
    //we just need an access to Dao.
    public abstract NoteDao noteDao();


    //(singleton)
    //we use synchronize 34an n5ly el method dy t run mra wa7da bs in multiple threads
    //y3ny law fy kza instance m elclass w run elmethod dy hyw2fha l8aya m t5ls w b3den y48lha
    public static synchronized NoteDatabase getInstance(Context context){
        if (instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    NoteDatabase.class,"notes_database")
                    .fallbackToDestructiveMigration() //this can delete all database when updating the database without changing the version num
                    .addCallback(callback)
                    .build();
        } //it will return the existing instance if its not null
        return instance;
    }


    private static Callback callback = new Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void>{
        private NoteDao noteDao;

        public PopulateDbAsyncTask(NoteDatabase db) {
            this.noteDao = db.noteDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            noteDao.insert(new Note("title1", "desc1", 1));
            noteDao.insert(new Note("title2", "desc2", 2));
            noteDao.insert(new Note("title3", "desc3", 3));
            noteDao.insert(new Note("title4", "desc4", 4));
            return null;
        }
    }


}
